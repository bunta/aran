<?php

class basedb {

    private $pdo;

    public function __construct() {
        $this->dbconn();
    }

    private function dbconn() {
        $config = manager::getConfig();
        $this->pdo = new PDO($config["db"]["connectionString"], $config["db"]["username"], $config["db"]["password"]);
        $this->pdo->query('SET CHARSET ' . $config["db"]["charset"]);
        $this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    }

    public function query($sql) {
        $stmt = $this->pdo->query($sql);
        $result = $stmt->fetchAll();
        $stmt->closeCursor();
        return $result;
    }

    public function update($sql) {
        $stmt = $this->pdo->query($sql);
    }

}

?>
