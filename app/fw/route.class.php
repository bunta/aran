<?php

class route {

    private $manager;
    private $routeController;
    private $routeAction;

    public function __construct($manager) {
        $this->manager = $manager;
    }

    function run() {
        $this->getRouteController();
        //dodac sprawdzenie poprawnosci routera
        if (file_exists("./app/controller/" . $this->routeController . ".php")) {
            include "./app/controller/" . $this->routeController . ".php";
        } else {
   
            echo "Brak takiego kontrolera.";
            return 0;
        }
        $class = $this->routeController;
        $controller = new $class($this->manager);

        $action = $this->routeAction;
        if (method_exists($controller, $action)) {
            $controller->$action();
        } else {
 
            echo "Brak takiej metody.";
            return 0;
        }
    }

    function getRouteController() {
        $config = manager::getConfig();
        if (empty($_GET['route'])) {

            $this->routeController = $config["defalutController"] . "Controller";
            $this->routeAction = "index";
        } else {
            $route = explode("/", $_GET['route']);
            $this->routeController = $route[0] . "Controller";
            if (isset($route[1]) && $route[1] != "") {
                $this->routeAction = $route[1];
            } else {
                $this->routeAction = "index";
            }
        }
    }

    function makeLink($Controller, $Action, $Name) {
        if ($Action != "") {
            $Action = "/" . $Action;
        }
        return "<a href=index.php?route=" . $Controller . "" . $Action . ">" . $Name . "</a>";
    }

}

?>
