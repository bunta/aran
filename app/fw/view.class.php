<?php

class view {
 
    private $manager;
    private $vars = array();
    private $view;

    public function __construct($manager) {
        $this->manager = $manager;
    }

    public function __set($name, $value) {
        $this->vars[$name] = $value;
    }

    public function renderAll($view) {
        $this->view = $view;
        include "./app/view/template.php";
    }

    public function renderView() {
        //dodac sprawdzenie czy plik istnieje
        include "./app/view/" . $this->view . ".php";
    }

}

?>
