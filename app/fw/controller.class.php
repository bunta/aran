<?php

abstract class baseController {

    protected $manager;

    public function __construct($manager) {

        $this->manager = $manager;
    }

    abstract function index();
}

?>
