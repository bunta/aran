<?php

include "./app/fw/controller.class.php";
include "./app/fw/view.class.php";
include "./app/fw/manager.class.php";
include "./app/fw/route.class.php";
include "./app/fw/db.class.php";

$manager = new manager();
$manager->route = new route($manager);
$manager->view = new view($manager);

function __autoload($class_name) {
    //dodac sprawdzanie poprawnosci
    include("./app/model/" . $class_name . ".php");
}

?>
