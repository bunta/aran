<?php

Class cvController Extends baseController {

    public function index() {
        $model = new cv();
        $result = $model->show("1");

        $this->manager->view->head = "CV";
        $this->manager->view->dane = $result;
        $this->manager->view->renderAll("cv/index");
    }

}

?>
