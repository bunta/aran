<?php

Class mainController Extends baseController {

    public function index() {
        $this->manager->view->head = "Home";
        $this->manager->view->title = "<u>Home</u>";
        $this->manager->view->h1 = "<h1>Witam</h2>";
        $this->manager->view->text = "<p>Jest to aplikacja pokazowa stworzona na celu promocji swojej osoby na stanowisko Programisty PHP.</p>";
        $this->manager->view->foot = "<p>Pozdrawiam<BR>Artur Anyszek</p>";
        $this->manager->view->renderAll("main/index");
    }

}

?>
