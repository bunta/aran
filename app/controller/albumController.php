<?php

Class albumController Extends baseController {

    public function index() {
        $this->all();
    }

    public function all() {
        $model = new album();
        $result = $model->all();

        $this->manager->view->head = "Wszystkie elementy.";
        $this->manager->view->title = "<u>Wszystkie elementy.</u>";
        $this->manager->view->dane = $result;
        $this->manager->view->renderAll("album/all");
    }

    public function edit() {
        if (isset($_POST['nazwa'])) {
            $dane = array('id' => $_POST['id'], 'nazwa' => $_POST['nazwa'], 'email' => $_POST['email'], 'opis' => $_POST['opis']);
            $model = new album();
            $result = $model->edit($dane);
            $this->all();
        } else {
            if (isset($_POST['id'])) {
                $id = $_POST['id'];
                $model = new album();
                $result = $model->count($id);
                if ($result > 0) {
                    $result = $model->getAlbum($id);
                    $this->manager->view->head = "Edycja jednego elementu.";
                    $this->manager->view->dane = $result;
                    $this->manager->view->route = 'album/edit';
                    $this->manager->view->renderAll("album/edit");
                    return 1;
                }
                else
                    echo 'Brak takiego id.';
            }
            echo "Brak id.";
        }
    }

    public function add() {
        if (isset($_POST['nazwa'])) {
            $model = new album();
            $dane = array('id' => $_POST['id'], 'nazwa' => $_POST['nazwa'], 'email' => $_POST['email'], 'opis' => $_POST['opis']);
            $result = $model->insert($dane);
            $this->all();
        } else {
            $this->manager->view->head = "Dodanie nowego elementu.";
            $this->manager->view->dane = array('0' => array('id' => '', 'nazwa' => '', 'email' => '', 'opis' => ''));
            $this->manager->view->route = 'album/add';
            $this->manager->view->renderAll("album/edit");
        }
    }

    public function delete() {
        if (isset($_POST['id'])) {
            $id = (int) $_POST['id'];
            $model = new album();
            $model->delAlbum($id);
        }
        $this->all();
    }

}

?>
