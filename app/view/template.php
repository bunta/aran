<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <title><? echo $this->vars['head'] ?></title>
        <link href="style.css" rel="stylesheet" type="text/css">
    </head>
    <body>
        <div id="slide">
            <p>Autor: Artur Anyszek<BR>
                Na potrzeby prezentacji</p>
        </div>

        <div id="head-container">
            <div id="header">
                <h1>
                    Aplikacja modelowa
                </h1>
            </div>
        </div>
        <div id="navigation-container">
            <div id="navigation">
                <ul>
                    <?php
                    echo "<li>" . $this->manager->route->makeLink("main", "", "Home") . "</li>";
                    echo "<li>" . $this->manager->route->makeLink("album", "all", "Wszystkie elementy") . "</li>";
                    echo "<li>" . $this->manager->route->makeLink("album", "add", "Dodaj nowy element") . "</li>";
                    echo "<li>" . $this->manager->route->makeLink("cv", "", "CV") . "</li>";
                    ?>
                </ul>
            </div>
        </div>
        <div id="content-container">
            <div id="content-container2">
                <div id="content-container3">
                    <div id="content"> 
                        <?php $this->renderView() ?>
                    </div>
                </div>
            </div>
            <div id="footer-container">
                <div id="footer">
                    Artur Anyszek, 2013
                </div>
            </div>
        </div>
    </body>
</html>


