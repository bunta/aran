<script type="text/javascript"
src="http://ajax.googleapis.com/ajax/libs/jquery/1.3.2/jquery.min.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
        $('.editform').submit(function() 
        {
            var $this = $(this);

            if($('input[name=nazwa]').val().length<=0 || $('input[name=nazwa]').val().length>30)
            {
                showerror("Popraw pole NAZWA.");
                return false; 
            }
            if($('input[name=email]').val().length<=0 || $('input[name=email]').val().length>50)
            {
                var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
                if(emailReg.test( $('input[name=email]').val())) 
                {
                    showerror("Wpisz E-MAIL.");
                }
                return false; 
            }
            var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
            if(!emailReg.test( $('input[name=email]').val())) 
            {
                showerror("Popraw pole E-MAIL.");
                return false;  
            }
            
            showerror("ok");
            return true;               
        })
        
        function showerror(name){
            $('#error').html(name).css('color', 'red').hide().show(400);
        }
    }); 
</script>
<p id="error"> </p>
<table class="dane">
    <tr >
        <th>
            Nazwa
        </th>
        <th>
            e-mail
        </th>
        <th>
            Opis
        </th>
        <th>
            Akceptuj
        </th>
    </tr>
    <?php
    echo '<FORM name="edit" action="index.php?route=' . $this->vars['route'] . '" method="post" class="editform">';
    echo '<input type="hidden" name="id" value=' . $this->vars['dane'][0]['id'] . '>';
    echo '<td><input type="text" name="nazwa" value=' . $this->vars['dane'][0]['nazwa'] . '></td>';
    echo '<td><input type="mail" name="email" value=' . $this->vars['dane'][0]['email'] . '></td>';
    echo '<td><textarea name="opis">' . $this->vars['dane'][0]['opis'] . '</textarea></td>';
    echo '<td><input type="submit" value="Akceptuj" /></td>';
    echo '</FORM>'
    ?>
</table>